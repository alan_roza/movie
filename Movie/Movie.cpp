// DealTest.cpp : Este arquivo contém a função 'main'. A execução do programa começa e termina ali.
//

#include "pch.h"

static void show_usage()
{
	std::cout << "Options:\n"
			  << "-h | --help\t\tPrints a list of application options.\n"
			  << "-s | --search [option]\tSearch by media title.\n"
		  	  << "-t | --type [option]\tReturns the selected media type. (movie, series, episode)\n"
			  << std::endl;
}

static void write_json_file(std::string argOpt1, std::string argOpt2) {
	FILE *fp;
	fopen_s(&fp, "omdb.json", "wb");
	curl_global_init(CURL_GLOBAL_ALL);
	CURL *curl = curl_easy_init();
	std::string strURL = "http://www.omdbapi.com/?i=tt3896198&apikey=7dbbaacc";
	strURL.append("&s=");
	strURL.append(argOpt1);
	if (argOpt2.compare("") != 0)
	{
		strURL.append("&type=");
		strURL.append(argOpt2);
	}
	boost::replace_all(strURL, " ", "%20");
	if (curl) {
		curl_easy_setopt(curl, CURLOPT_URL, strURL.c_str());

		curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);

		/* use a GET to fetch this */
		curl_easy_setopt(curl, CURLOPT_HTTPGET, 1L);

		/* Perform the request */
		curl_easy_perform(curl);
	}
	fclose(fp);
}

static void read_json_file(std::string argOpt1, std::string argOpt2) {
	FILE *fp;
	fopen_s(&fp, "omdb.json", "rb");
	char readBuffer[65536];
	rapidjson::FileReadStream is(fp, readBuffer, sizeof(readBuffer));
	rapidjson::Document d;
	d.ParseStream(is);

	rapidjson::Value & val = d["Search"];
	//int totalRes = val.GetInt();

	for (rapidjson::SizeType i = 0; i < val.Size(); i++ )
	{
		std::cout << "Movie " << i + 1 << "\n";
		rapidjson::Value & valMovie = val[i]["Title"];
		std::cout << "Title - "
			<< valMovie.GetString() << "\n";
		valMovie = val[i]["Year"];
		std::cout << "Year - "
			<< valMovie.GetString() << "\n";
		valMovie = val[i]["Type"];
		std::cout << "Type - "
			<< valMovie.GetString() << "\n";
		valMovie = val[i]["Poster"];
		std::cout << "Poster - "
			<< valMovie.GetString() << "\n";
		std::cout << "\n";
	}

	//all informations
	/*rapidjson::Value & val = d["Title"];
	std::cout << "Title - "
		<< val.GetString() << "\n";
	val = d["Year"];
	std::cout << "Year - "
		<< val.GetString() << "\n";
	val = d["Rated"];
	std::cout << "Rated - "
		<< val.GetString() << "\n";
	val = d["Released"];
	std::cout << "Released - "
		<< val.GetString() << "\n";
	val = d["Runtime"];
	std::cout << "Runtime - "
		<< val.GetString() << "\n";
	val = d["Genre"];
	std::cout << "Genre - "
		<< val.GetString() << "\n";
	val = d["Director"];
	std::cout << "Director - "
		<< val.GetString() << "\n";
	val = d["Writer"];
	std::cout << "Writer - "
		<< val.GetString() << "\n";
	val = d["Actors"];
	std::cout << "Actors - "
		<< val.GetString() << "\n";
	val = d["Plot"];
	std::cout << "Plot - "
		<< val.GetString() << "\n";
	val = d["Language"];
	std::cout << "Language - "
		<< val.GetString() << "\n";
	val = d["Country"];
	std::cout << "Country - "
		<< val.GetString() << "\n";
	val = d["Awards"];
	std::cout << "Awards - "
		<< val.GetString() << "\n";
	val = d["Poster"];
	std::cout << "Poster - "
		<< val.GetString() << "\n";
	std::cout << "Ratings - \n";
	val = d["Ratings"][0]["Source"];
	std::cout << val.GetString() << ": ";
	val = d["Ratings"][0]["Value"];
	std::cout << val.GetString() << "\n";
	val = d["Ratings"][1]["Source"];
	std::cout << val.GetString() << ": ";
	val = d["Ratings"][1]["Value"];
	std::cout << val.GetString() << "\n";
	val = d["Ratings"][2]["Source"];
	std::cout << val.GetString() << ": ";
	val = d["Ratings"][2]["Value"];
	std::cout << val.GetString() << "\n";
	val = d["Type"];
	std::cout << "Type - "
		<< val.GetString() << "\n";
	val = d["DVD"];
	std::cout << "DVD - "
		<< val.GetString() << "\n";
	val = d["BoxOffice"];
	std::cout << "BoxOffice - "
		<< val.GetString() << "\n";
	val = d["Production"];
	std::cout << "Production - "
		<< val.GetString() << "\n";
	val = d["Website"];
	std::cout << "Website - "
		<< val.GetString() << "\n"
		<< std::endl;*/
	fclose(fp);
}

int main(int argc, char* argv[])
{
	std::string arg1 = argv[1], argOpt1, arg2, argOpt2, argAux;
	if (argc >= 2 && arg1 != "-h" && arg1 != "--help")
	{
		bool bOpt1 = false, bOpt2 = false;
		for (int i = 1; i < argc; i++)
		{
			argAux = argv[i];
			if (!bOpt1 && (argAux.compare("-s") == 0 || argAux.compare("--search") == 0))
			{
				arg1 = argAux;
				bOpt1 = true;
				bOpt2 = false;
			}
			else if (!bOpt2 && (argAux.compare("-t") == 0 || argAux.compare("--type") == 0))
			{
				arg2 = argAux;
				bOpt1 = false;
				bOpt2 = true;
			}
			else if (bOpt1 && !bOpt2)
			{
				argOpt1.append(argAux);
				argOpt1.append(" ");
			}
			else if (bOpt2 && !bOpt1)
			{
				argOpt2.append(argAux);
				argOpt2.append(" ");
			}
		}
		if (argOpt1.size() > 0)
			argOpt1.resize(argOpt1.length() - 1);
		if(argOpt2.size() > 0)
			argOpt2.resize(argOpt2.length() - 1);
	}
	else {
		show_usage();
		return 1;
	}

	if ((arg1 == "-s" || arg1 == "--search") && (arg2 == "-t" || arg2 == "--type"))
	{
		// write json file from api (using curl)
		write_json_file(argOpt1, argOpt2);
		// read json file (using rapidjson)
		read_json_file(argOpt1, argOpt2);
	}
	else if (arg1 == "-s" || arg1 == "--search")
	{
		// write json file from api (using curl)
		write_json_file(argOpt1, "");
		// read json file (using rapidjson)
		read_json_file(argOpt1, "");
	}
}
